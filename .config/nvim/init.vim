" Install Vim-Plug
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
     \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Add Plugins
call plug#begin()

" Supertab
Plug 'ervandew/supertab'

" YouCompleteMe
Plug 'Valloric/YouCompleteMe'
let g:ycm_global_ycm_extra_conf = '~/.config/nvim/.ycm_extra_conf.py'

" UltiSnips
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'

" Auto Pairs
Plug 'jiangmiao/auto-pairs'

call plug#end()

" Personal Configs
" Set plugin type indenting
filetype plugin indent on
" Enable Syntax Highlighting
syntax enable
" Set indentation
set cindent
set shiftwidth=4
set tabstop=4
set expandtab

" C++ Template
if has("autocmd")
  augroup templates
    autocmd BufNewFile *.cpp 0r ~/.config/nvim/templates/comp.cpp
  augroup END
endif

" Set numbering
set number
set relativenumber
