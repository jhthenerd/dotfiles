# run pfetch
~/bin/pfetch/pfetch

# make bash prompt nicer
HOST='\[\033[02;36m\]\h'; HOST=' '$HOST
TIME='\[\033[01;31m\]\t \[\033[01;32m\]'
LOCATION=' \[\033[01;34m\]`pwd | sed "s#\(/[^/]\{1,\}/[^/]\{1,\}/[^/]\{1,\}/\).*\(/[^/]\{1,\}/[^/]\{1,\}\)/\{0,1\}#\1_\2#g"`\033[0m'
PS1=$TIME$USER$HOST$LOCATION'\n\$ '

# config dotfiles
alias config='/usr/bin/git --git-dir=/home/jhuang/.cfg/ --work-tree=/home/jhuang'

# Set editor to nvim
VISUAL=nvim; export VISUAL EDITOR=nvim; export EDITOR

# setup linuxbrew
eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
